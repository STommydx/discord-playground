FROM node:16 as builder

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install
COPY ./ ./
RUN npm run build

FROM node:16

COPY package.json package-lock.json ./
RUN npm install --production
COPY --from=builder /app/dist/ /app/dist/

CMD ["/bin/bash", "-c", "node /app/dist/deploy_command.js && node /app/dist/index.js"]
