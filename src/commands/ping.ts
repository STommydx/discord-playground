import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction, CacheType } from "discord.js";
import { Command, DefaultCommandBuilder } from "./command";

export class PingCommand extends Command {
  getCommandBuilder(): SlashCommandBuilder {
    return new PingCommandBuilder().getCommandBuilder();
  }

  async execute(interaction: CommandInteraction<CacheType>): Promise<void> {
    await interaction.reply("Pong!");
  }
}

export class PingCommandBuilder extends DefaultCommandBuilder {
  constructor() {
    super("ping", "Replies with pong!");
  }
}
