import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction, CacheType } from "discord.js";
import { Command, DefaultCommandBuilder } from "./command";

export class ServerCommand extends Command {
  getCommandBuilder(): SlashCommandBuilder {
    return new ServerCommandBuilder().getCommandBuilder();
  }

  async execute(interaction: CommandInteraction<CacheType>): Promise<void> {
    await interaction.reply(
      `Server name: ${interaction.guild?.name}\nTotal members: ${interaction.guild?.memberCount}`
    );
  }
}

export class ServerCommandBuilder extends DefaultCommandBuilder {
  constructor() {
    super("server", "Replies with server info!");
  }
}
