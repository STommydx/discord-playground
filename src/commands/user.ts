import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction, CacheType } from "discord.js";
import { Command, DefaultCommandBuilder } from "./command";

export class UserCommand extends Command {
  getCommandBuilder(): SlashCommandBuilder {
    return new UserCommandBuilder().getCommandBuilder();
  }

  async execute(interaction: CommandInteraction<CacheType>): Promise<void> {
    await interaction.reply(
      `Your tag: ${interaction.user.tag}\nYour id: ${interaction.user.id}`
    );
  }
}

export class UserCommandBuilder extends DefaultCommandBuilder {
  constructor() {
    super("user", "Replies with user info!");
  }
}
