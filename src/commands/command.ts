import {
  SlashCommandBuilder,
  SlashCommandSubcommandsOnlyBuilder,
} from "@discordjs/builders";
import {
  CacheType,
  CommandInteraction,
  ModalSubmitInteraction,
} from "discord.js";

export abstract class Command {
  abstract execute(interaction: CommandInteraction<CacheType>): Promise<void>;

  async handleModalSubmit(
    interaction: ModalSubmitInteraction<CacheType>
  ): Promise<void> {}

  abstract getCommandBuilder():
    | SlashCommandBuilder
    | SlashCommandSubcommandsOnlyBuilder;
}

export abstract class DefaultCommandBuilder {
  private name: string;
  private description: string;

  constructor(name: string, description: string) {
    this.name = name;
    this.description = description;
  }

  getCommandBuilder(): SlashCommandBuilder {
    return new SlashCommandBuilder()
      .setName(this.name)
      .setDescription(this.description);
  }
}

export abstract class CommandBuilder {
  abstract getCommandBuilder():
    | SlashCommandBuilder
    | SlashCommandSubcommandsOnlyBuilder;
}
