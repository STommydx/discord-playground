import {
  SlashCommandBuilder,
  SlashCommandSubcommandsOnlyBuilder,
} from "@discordjs/builders";
import {
  CommandInteraction,
  CacheType,
  Modal,
  ModalSubmitInteraction,
  TextInputComponent,
  MessageActionRow,
  ModalActionRowComponent,
} from "discord.js";
import { Repository } from "typeorm";
import { CreateProblem, Problem } from "../entities/problem";
import { Command, CommandBuilder } from "./command";

export class ProblemService {
  private readonly problemRepository: Repository<Problem>;

  constructor(problemRepository: Repository<Problem>) {
    this.problemRepository = problemRepository;
  }

  async get(): Promise<Problem[]> {
    return await this.problemRepository.find();
  }

  async getOne(problemId: number): Promise<Problem | null> {
    return await this.problemRepository.findOne({ where: { id: problemId } });
  }

  async create(problem: CreateProblem): Promise<Problem> {
    return await this.problemRepository.save(
      this.problemRepository.create(problem)
    );
  }

  async delete(problemId: number) {
    return await this.problemRepository.delete({ id: problemId });
  }
}

export class ProblemCommand extends Command {
  private readonly problemService: ProblemService;

  constructor(problemService: ProblemService) {
    super();
    this.problemService = problemService;
  }

  async execute(interaction: CommandInteraction<CacheType>): Promise<void> {
    switch (interaction.options.getSubcommand()) {
      case "get":
        await this.get(interaction);
        break;
      case "get_one":
        await this.getOne(interaction);
        break;
      case "create":
        await this.create(interaction);
        break;
      case "delete":
        await this.delete(interaction);
        break;
    }
  }

  async handleModalSubmit(
    interaction: ModalSubmitInteraction<CacheType>
  ): Promise<void> {
    switch (interaction.customId) {
      case "createProblem":
        await this.createSubmit(interaction);
        break;
    }
  }

  async get(interaction: CommandInteraction<CacheType>): Promise<void> {
    const problems = await this.problemService.get();
    await interaction.reply(JSON.stringify(problems));
  }

  async getOne(interaction: CommandInteraction<CacheType>): Promise<void> {
    const problem = await this.problemService.getOne(
      interaction.options.getInteger("problem_id", true)
    );
    await interaction.reply(JSON.stringify(problem));
  }

  async create(interaction: CommandInteraction<CacheType>): Promise<void> {
    const modal = new Modal()
      .setCustomId("createProblem")
      .setTitle("Create Problem");
    const cfidInput = new TextInputComponent()
      .setCustomId("cfid")
      .setLabel("Codeforces Problem ID")
      .setStyle("SHORT");
    const difficultyInput = new TextInputComponent()
      .setCustomId("difficulty")
      .setLabel("Difficulty")
      .setStyle("SHORT");
    modal.addComponents(
      new MessageActionRow<ModalActionRowComponent>().addComponents(cfidInput),
      new MessageActionRow<ModalActionRowComponent>().addComponents(
        difficultyInput
      )
    );
    await interaction.showModal(modal);
  }

  async createSubmit(
    interaction: ModalSubmitInteraction<CacheType>
  ): Promise<void> {
    const problem = await this.problemService.create({
      cfid: interaction.fields.getTextInputValue("cfid"),
      difficulty: parseInt(interaction.fields.getTextInputValue("difficulty")),
    });
    await interaction.reply(JSON.stringify(problem));
  }

  async delete(interaction: CommandInteraction<CacheType>): Promise<void> {
    await this.problemService.delete(
      interaction.options.getInteger("problem_id", true)
    );
    await interaction.reply("OK");
  }

  getCommandBuilder():
    | SlashCommandBuilder
    | SlashCommandSubcommandsOnlyBuilder {
    return new ProblemCommandBuilder().getCommandBuilder();
  }
}

export class ProblemCommandBuilder extends CommandBuilder {
  getCommandBuilder():
    | SlashCommandBuilder
    | SlashCommandSubcommandsOnlyBuilder {
    return new SlashCommandBuilder()
      .setName("problem")
      .setDescription("problem CRUD")
      .addSubcommand((subcommand) =>
        subcommand.setName("create").setDescription("Create a problem.")
      )
      .addSubcommand((subcommand) =>
        subcommand.setName("get").setDescription("Get all problems.")
      )
      .addSubcommand((subcommand) =>
        subcommand
          .setName("get_one")
          .setDescription("Get a single problem.")
          .addIntegerOption((option) =>
            option
              .setName("problem_id")
              .setDescription("The problem ID")
              .setRequired(true)
          )
      )
      .addSubcommand((subcommand) =>
        subcommand
          .setName("delete")
          .setDescription("Delete a problem.")
          .addIntegerOption((option) =>
            option
              .setName("problem_id")
              .setDescription("The problem ID")
              .setRequired(true)
          )
      );
  }
}
