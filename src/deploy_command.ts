import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import { CommandBuilder } from "./commands/command";
import { PingCommandBuilder } from "./commands/ping";
import { ProblemCommandBuilder } from "./commands/problem";
import { ServerCommandBuilder } from "./commands/server";
import { UserCommandBuilder } from "./commands/user";

const commandBuilders: CommandBuilder[] = [
  new PingCommandBuilder(),
  new ServerCommandBuilder(),
  new UserCommandBuilder(),
  new ProblemCommandBuilder(),
];

const commands = commandBuilders.map((commandBuilder) =>
  commandBuilder.getCommandBuilder().toJSON()
);

const rest = new REST({ version: "9" }).setToken(
  process.env.DISCORD_BOT_TOKEN || ""
);

rest
  .put(
    Routes.applicationGuildCommands(
      process.env.DISCORD_APPLICATION_ID || "",
      process.env.DISCORD_GUILD_ID || ""
    ),
    { body: commands }
  )
  .then(() => console.log("Successfully registered application commands."))
  .catch(console.error);
