import { Client, Intents } from "discord.js";
import { Command } from "./commands/command";
import { PingCommand } from "./commands/ping";
import { ProblemCommand, ProblemService } from "./commands/problem";
import { ServerCommand } from "./commands/server";
import { UserCommand } from "./commands/user";
import { AppDataSource } from "./datasource";
import { Problem } from "./entities/problem";

async function main() {
  await AppDataSource.initialize();

  const problemService = new ProblemService(
    AppDataSource.getRepository(Problem)
  );

  const commands: Command[] = [
    new PingCommand(),
    new ServerCommand(),
    new UserCommand(),
    new ProblemCommand(problemService),
  ];

  const commandMap: { [key: string]: Command } = {};

  commands.forEach((command) => {
    commandMap[command.getCommandBuilder().name] = command;
  });

  // Create a new client instance
  const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

  // When the client is ready, run this code (only once)
  client.once("ready", () => {
    console.log("Ready!");
  });

  client.on("interactionCreate", async (interaction) => {
    if (!interaction.isCommand()) return;

    const { commandName } = interaction;

    if (commandMap[commandName]) {
      await commandMap[commandName].execute(interaction);
    }
  });

  client.on("interactionCreate", async (interaction) => {
    if (!interaction.isModalSubmit()) return;
    await Promise.all(
      commands.map((command) => command.handleModalSubmit(interaction))
    );
  });

  // Login to Discord with your client's token
  client.login(process.env.DISCORD_BOT_TOKEN);
}

main();
