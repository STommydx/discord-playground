import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Problem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  cfid: string;

  @Column()
  difficulty: number;
}

export class CreateProblem {
  @Column()
  cfid: string;

  @Column()
  difficulty: number;
}
